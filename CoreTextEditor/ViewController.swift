//
//  ViewController.swift
//  CoreTextEditor
//
//  Created by Sean Cheng on 21/11/2017.
//  Copyright © 2017 Sean Cheng. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController
{
	// 用于获得用户输入情况的 Custom Class
	var textStorage : NSTextStorage = NSTextStorage(string: "Hello World")

	// 用于访问数据库的 Container
	var dataManager = NSPersistentContainer(name: "Model")
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
		let container = NSTextContainer(size: view.frame.size)
		let manager = NSLayoutManager()
		manager.textStorage = textStorage
		manager.addTextContainer(container)
		view = UITextView(frame: view.frame, textContainer: container)
	}

	override func didReceiveMemoryWarning()
	{
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
}

