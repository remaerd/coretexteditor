//
//  Paragraph+CoreDataProperties.swift
//  CoreTextEditor
//
//  Created by Sean Cheng on 21/11/2017.
//  Copyright © 2017 Sean Cheng. All rights reserved.
//
//

import Foundation
import CoreData

extension Paragraph
{
	@nonobjc public class func fetchRequest() -> NSFetchRequest<Paragraph>
	{
		return NSFetchRequest<Paragraph>(entityName: "Paragraph")
	}

	// 段落内容
	@NSManaged public var content: String?
	
	// 本段落的文字的 Attributes
	@NSManaged public var innerStyle: NSObject?
	
	// 本段落的类型 Header / Body 等等
	@NSManaged public var type: Int16

}
