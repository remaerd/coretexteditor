//
//  Paragraph+CoreDataClass.swift
//  CoreTextEditor
//
//  Created by Sean Cheng on 21/11/2017.
//  Copyright © 2017 Sean Cheng. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Paragraph)
public class Paragraph: NSManagedObject
{
	// 本段落的类型
	enum ParagraphType
	{
		case header(depth: Int)
		case body
		case numericList
		case list
		case quote
	}
	
	var paragraphType: ParagraphType = .body
}
