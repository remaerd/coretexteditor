//
//  TextStorage.swift
//  CoreTextEditor
//
//  Created by Sean Cheng on 21/11/2017.
//  Copyright © 2017 Sean Cheng. All rights reserved.
//

import UIKit

// 问题的核心在这里。能够根据用户的输入来调整数据库
class TextStorage: NSTextStorage
{
	var attributedString: NSAttributedString
	
	override init(string str: String)
	{
		attributedString = NSAttributedString(string: str)
		super.init()
	}
	
	required init?(coder aDecoder: NSCoder)
	{
		fatalError("init(coder:) has not been implemented")
	}
	
	override func replaceCharacters(in range: NSRange, with attrString: NSAttributedString)
	{
		// TODO: 在这里获得用户如何修改内容，并记录到数据库内。
		super.replaceCharacters(in: range, with: attrString)
		print(self)
	}
}
